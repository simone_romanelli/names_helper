require 'coveralls'
Coveralls.wear!
require 'simplecov'
SimpleCov.start

require 'names_helper'

RSpec.configure do |c|
  c.include NamesHelper
end

RSpec.configure do |config|
  config.expect_with :rspec do |c|
    c.syntax = [:should, :expect]
  end
end
