require 'spec_helper'

describe NamesHelper do
  it 'NamesHelper has titles constants' do
    expect NamesHelper::TITLES.should_not be_nil
  end

  it 'Titles should be an array of strings' do
    NamesHelper::TITLES.each do |title|
      expect title.class.should eq String
    end
  end

  describe '#compose_fullname' do
    it 'return an array' do
      expect compose_fullname('MCLAUGHLIN FORD', 'PHILLIP').class
        .should eq Array
    end

    it 'return the correct surname when is composed from more words' do
      expect compose_fullname('MCLAUGHLIN BIG FORD', 'PHILLIP')[0]
        .should eq 'Mclaughlin Big Ford'
    end

    it 'return the correct name when is composed from more words' do
      expect compose_fullname('MCLAUGHLIN BIG FORD', 'PHILLIP JUNIOR')[1]
        .should eq 'Phillip Junior'
    end
  end

  describe '#remove_title_from_name' do
    it 'remove the title from the name' do
      expect remove_title_from_name('MR PHILLIP JUNIOR MR')
        .should eq 'PHILLIP JUNIOR'
    end

    it 'do not modify the name if it contains some titles' do
      expect remove_title_from_name('INFINIT PHILLIP JUNIOR')
        .should eq 'INFINIT PHILLIP JUNIOR'
    end
  end

  describe '#format_name' do
    before(:each) do
      @phillip_mr_fullname = 'MCLAUGHLIN FORD/PHILLIP MR'
      @mr_phillip_fullname = 'MCLAUGHLIN FORD/MR PHILLIP'
    end

    it 'should return and array with legth equal to 2' do
      expect format_name(@mr_phillip_fullname).size.should eq 2
    end

    it 'first element of the result should be the family name' do
      expect format_name(@mr_phillip_fullname)[0]
        .should eq 'Mclaughlin Ford'
    end

    it 'second element of the result should be the name' do
      expect format_name(@mr_phillip_fullname)[1]
        .should eq 'Phillip'
    end

    it "second element of the result should
        be the name also when title is first" do
      expect format_name(@mr_phillip_fullname)[1]
        .should eq 'Phillip'
    end
  end
end
