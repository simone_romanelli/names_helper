# NamesHelper #

[ ![Codeship Status for simone_romanelli/names_helper](https://codeship.com/projects/b5956a10-3abe-0134-1ea7-06456b66cf53/status?branch=master)](https://codeship.com/projects/166399)

[![Coverage Status](https://coveralls.io/repos/bitbucket/simone_romanelli/names_helper/badge.svg?branch=master)](https://coveralls.io/bitbucket/simone_romanelli/names_helper?branch=master)

This simple helper provide a method to parse a full name passed as argument and return an humanized version of it.

Full name must be a string and have the following format:

``` $SURMANE/$NAME $TITLE```
or
``` $SURNAME/$TITLE $NAME```

### How to use

```ruby
# Require the file
require 'names_helper'

#Include in your class
include NamesHelper

# Format name using
format_name('MCLAUGHLIN FORD/PHILLIP MR')
```