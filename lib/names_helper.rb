# This helper provide static method to format names
module NamesHelper
  TITLES = %w(MRS MR MS CHD INF).freeze

  def format_name(full_name)
    validate_titles
    user = split_full_name(full_name)
    user_name = remove_title_from_name(user[:name])
    compose_fullname(user[:family_name], user_name)
  end

  def split_full_name(full_name_array)
    splitted_name = full_name_array.split('/')
    {
      family_name: get_family_name(splitted_name),
      name: get_name(splitted_name)
    }
  end

  def compose_fullname(family_name, name)
    [
      family_name.gsub(/\w+/, &:capitalize),
      name.gsub(/\w+/, &:capitalize)
    ]
  end

  def remove_title_from_name(name)
    (name.split(' ') - TITLES).join(' ')
  end

  def get_family_name(full_name_array)
    full_name_array[0]
  end

  def get_name(full_name_array)
    full_name_array[1]
  end

  def validate_titles
    TITLES.each { |title| raise ArgumentError unless title.class == String }
  end
end
